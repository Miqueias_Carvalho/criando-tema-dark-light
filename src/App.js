import GlobalStyle from "./styles/global";
import { AppContainer, MainSection } from "./components/app.style";
import { ThemeProvider } from "styled-components";
import { themes } from "./styles/themes";
import { Button } from "./components/button.style";
import { useState, useCallback } from "react";

function App() {
  const [currentTheme, setCurrentTheme] = useState("light");

  const getOpositeTheme = useCallback(
    () => (currentTheme === "light" ? "dark" : "light"),
    [currentTheme]
  );

  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={themes[currentTheme]}>
        <AppContainer>
          <MainSection>
            <h1>You are in {currentTheme} mode</h1>
            <Button onClick={() => setCurrentTheme(getOpositeTheme())}>
              switch to {getOpositeTheme()} mode
            </Button>
          </MainSection>
        </AppContainer>
      </ThemeProvider>
    </>
  );
}

export default App;
